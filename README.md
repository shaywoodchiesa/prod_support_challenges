# Prod_Support_Challenges

#SQL Challenge 1

Assumptions:
-- When creating the table for this challenge, the name "apple_stocks_dataset" should be used.
-- Only the time and day at which the user wanted to run the interval was asked for, as asking for further inputs was not necessary (e.g endtime was not asked for as the interval would always be 5 hours)
-- @starttime = time at which user wanted interval to begin, @endtime = time at which interval would end (@starttime + 5 hours), startdate = day in which user wanted information on.

SQL used to change column headings:
alter table `databasename`.`apple_stocks_dataset` 
change column `<ticker>` `Ticker` text null,
change column `<date>` `Date` varchar(13) null,
change column `<open>` `Open` double null,
change column `<high>` `High` double null,
change column `<low>` `Low` double null,
change column `<close>` `Close` double null,
change column `<vol>` `Volume` double null;

Example run on Challenge 1 procedure:
-- call sp_chall1('20101011','09:00');

_______________________


#SQL Challenge 2

Assumptions
-- When creating the table for this challenge, the name "challenge2_dataset" should be used.
-- select distinct Date, (abs(Open-Close)) as 'Range'. The absolute function was used to negate negative values in my output.

Example run on Challenge 2 
-- call sp_chall2();