#!/usr/bin/python
import re

fileopen= open('hosts.real','r')
ipfile = open('ipfile.txt', 'w')
newhosts = open('newhosts.real','w')
reip=re.compile('([0-9]{1,3}.){3}[0-9]{1,3}')
remac=re.compile('[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}:[0-9a-z]{2}')

for line in fileopen:
    field = re.split('\s+',line)
    try:
        keepdata=field[0:field.index('#')]
        newhosts.write(' '.join(keepdata) + '\n')
    except ValueError:
        newhosts.write(' '.join(field) + '\n')

    ip=reip.search(line)
    if ip != None:
        ipfile.write('IP address: ' + ip.group(0) +'\n')
    mac=remac.search(line)
    if mac != None:
        print('Mac Address: ' + mac.group(0))

newhosts.close()
ipfile.close()
fileopen.close()
